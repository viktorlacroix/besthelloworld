﻿using System;

namespace BestHelloWorld
{
    class Program
    {
        /// <summary>
        /// Inspired by Microsoft.AspNetCore.Owin.OwinExtensions.UseOwin Method
        /// OWIN middleware
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            HelloWorld(s => w =>
            {
                Console.Write("Hello");
                return s(w);
            });
            Console.ReadKey();
        }

        static void HelloWorld(Func<Func<string, string>, Func<string, string>> func)
        {
            var hello = func(w =>
            {
                Console.Write(" ");
                return w;
            });
            var world = hello("World!");
            Console.WriteLine(world);
        }
    }
}
